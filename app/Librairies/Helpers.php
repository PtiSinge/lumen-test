<?php
/**
 * Created by PhpStorm.
 * User: maxfarras
 * Date: 16/02/2019
 * Time: 15:28
 */

namespace App\Librairies;

class Helpers
{
    public static function convertRecursivToUtf8($fileInfo)
    {
        array_walk_recursive($fileInfo, function (&$item, $key) {
            if (!mb_detect_encoding($item, 'utf-8', true)) {
                $item = utf8_encode($item);
            }
        });
        return $fileInfo;
    }
}