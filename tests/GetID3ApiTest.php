<?php

use Illuminate\Http\UploadedFile;

class GetID3ApiTest extends TestCase
{
    /**
     * Testing HTTP POST
     * @return void
     */
    public function testPost()
    {
        $response = $this->call('POST', '/api/v1/');
        $this->assertEquals(422, $response->status());
    }

    /**
     * Testing HTTP POST and returns the ID3 data in json
     * Note: in order to run the test again, make sure there is a "test_video.mp4" in the tests/test_video folder prior to run the test.
     * @return void
     */
    public function testPostMp4VideoAndGetId3()
    {
        $videoTestName = 'test_video.mp4';
        $videoTestPath = base_path().'/tests/test_videos/'.$videoTestName;
        $file = new UploadedFile($videoTestPath, $videoTestName, 'video/mp4', 446, null, true);
        $response = $this->call('POST', 'api/v1/', [], [], ['video' => $file]);
        $this->assertEquals(200, $response->status());
        json_decode($response->content(), true);
    }

    /**
     * Testing HTTP POST and returns the ID3 data in json
     * Note: in order to run the test again, make sure there is a "test_video.mkv" in the tests/test_video folder prior to run the test.
     * @return void
     */
    public function testPostMkvVideoAndGetId3()
    {
        $videoTestName = 'test_video.mkv';
        $videoTestPath = base_path().'/tests/test_videos/'.$videoTestName;
        $file = new UploadedFile($videoTestPath, $videoTestName, 'video/mkv', 446, null, true);
        $response = $this->call('POST', 'api/v1/', [], [], ['video' => $file]);
        $this->assertEquals(200, $response->status());
        json_decode($response->content(), true);
    }

    /**
     * Testing HTTP POST and returns the ID3 data in json
     * Note: in order to run the test again, make sure there is a "test_video.flv" in the tests/test_video folder prior to run the test.
     * @return void
     */
    public function testPostFlvVideoAndGetId3()
    {
        $videoTestName = 'test_video.flv';
        $videoTestPath = base_path().'/tests/test_videos/'.$videoTestName;
        $file = new UploadedFile($videoTestPath, $videoTestName, 'video/flv', 446, null, true);
        $response = $this->call('POST', 'api/v1/', [], [], ['video' => $file]);
        $this->assertEquals(200, $response->status());
        json_decode($response->content(), true);
    }

    /**
     * Testing HTTP POST and returns the ID3 data in json
     * Note: in order to run the test again, make sure there is a "test_video.3gp" in the tests/test_video folder prior to run the test.
     * @return void
     */
    public function testPost3gpVideoAndGetId3()
    {
        $videoTestName = 'test_video.3gp';
        $videoTestPath = base_path().'/tests/test_videos/'.$videoTestName;
        $file = new UploadedFile($videoTestPath, $videoTestName, 'video/3gp', 446, null, true);
        $response = $this->call('POST', 'api/v1/', [], [], ['video' => $file]);
        $this->assertEquals(200, $response->status());
        json_decode($response->content(), true);
    }

    /**
     * Testing HTTP POST without a video and returns a validation message
     * Note: in order to run the test again, make sure there is a "test_video.3gp" in the tests/test_video folder prior to run the test.
     * @return void
     */
    public function testPostNoVideoAndSeeValidationMessage()
    {
        $response = $this->call('POST', 'api/v1/', [], [], []);
        $this->assertEquals(422, $response->status());
        $this->assertEquals('["The video field is required."]',json_encode($response->original['video']));

        json_decode($response->content(), true);
    }
}
