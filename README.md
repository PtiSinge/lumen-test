# PHP Lumen Test
Undertaken by Max Farras


## Notes

### Set up
I have voluntarily included the vendor folder into this commit in case you were not using `composer` as it includes this necessary package:
`james-heinrich/getid3` 

### Usage
Make a HTTP POST request to `/api/v1` with a video file sent via form data  [video]

### PHP Unit testing
- The phpunit test will run successfully the first time.
- To repeat the phpunit tests make sure that all 4 test videos below are in the test_videos folder `app/tests/test_videos` before you run the tests.

`test_video.mp4`
`test_video.mkv`
`test_video.flv`
`test_video.3gp`