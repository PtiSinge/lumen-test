<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Librairies\Helpers;
use Illuminate\Http\Request;
use getID3;

class VideoController extends Controller
{
    /**
     * Post video file and returns a json of the id3 of the video
     * @param Request $video
     * @param getID3 $getID3
     * @return \Illuminate\Http\JsonResponse
     */
    public function postVideo(Request $request, getID3 $getID3)
    {
        $this->validate($request, ['video' => 'required']);
        $this->validate($request, ['video' => 'mimes:mp4,mkv,flv,3gp']);

        $video = $request->file('video');

        $ext = $video->getClientOriginalExtension();

        $fileName = str_random(32).'.'.$ext;

        $destinationPath = base_path().'/videos';

        $video->move($destinationPath,$fileName);

        $fileInfo = $getID3->analyze('../videos/'.$fileName);

        $fileInfo = Helpers::convertRecursivToUtf8($fileInfo);

        return response()->json($fileInfo,200);
    }
}